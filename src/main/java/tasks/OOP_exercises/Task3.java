package tasks.OOP_exercises;

public class Task3 {
    public static void main(String[] args) {
        Shape shape = new Shape("red", false);
        System.out.println(shape);
        Shape circle = new Circle("blue", true, 20);
        System.out.println(circle);
        Shape rectangle = new Rectangle("yellow", true, 20, 30);
        System.out.println(rectangle);
        Shape square = new Square("green", false, 40);
        System.out.println(square);
    }
}

class Shape {
    private String color;
    private boolean isFilled;
    public Shape() {
        this.color = "unknown";
        this.isFilled = false;
    }
    public Shape(String color, boolean isFilled) {
        this.color = color;
        this.isFilled = isFilled;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public boolean isFilled() {
        return isFilled;
    }
    public void setFilled(boolean filled) {
        isFilled = filled;
    }
    @Override
    public String toString() {
        return String.format("Shape with color of %s and %s",
                color, isFilled ? "filled" : "NotFilled");
    }
}
class Circle extends Shape {
    private float radius;
    public Circle(String color, boolean isFilled, float radius) {
        super(color, isFilled);
        this.radius = radius;
    }
    public float getRadius() {
        return radius;
    }
    public void setRadius(float radius) {
        this.radius = radius;
    }
    public float getArea() {
        return (float) (Math.PI * radius * radius);
    }
    public float getPerimeter() {
        return (float) (2 * Math.PI * radius);
    }
    @Override
    public String toString() {
        return String.format("Circle with radius=%f which is subclass off %s", radius, super.toString());
    }
}
class Rectangle extends Shape {
    protected double width, length;
    public Rectangle(String color, boolean isFilled, double width,
                     double length) {
        super(color, isFilled);
        this.width = width;
        this.length = length;
    }
    public double getWidth() {
        return width;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public double getLength() {
        return length;
    }
    public void setLength(double length) {
        this.length = length;
    }
    public float getArea() {
        return (float) (width * length);
    }
    public float getPerimeter() {
        return (float) (2 * width + 2 * length);
    }
    @Override
    public String toString() {
        return String.format("Rectangle with width=%f and length=%f which is subclass off %s", width, length,
        super.toString());
    }
}
class Square extends Rectangle {
    public Square(String color, boolean isFilled, double size) {
        super(color, isFilled, size, size);
    }
    @Override
    public void setWidth(double width) {
        super.setWidth(width);
        super.setLength(width);
    }
    @Override
    public void setLength(double length) {
        super.setWidth(width);
        super.setLength(length);
    }
    @Override
    public String toString() {
        return String.format("Square with width=%f and length=%f which is subclass off %s", width, length, super.toString());
    }
}
