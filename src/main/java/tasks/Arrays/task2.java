package tasks.Arrays;

public class task2 {
    public static void main(String[] args) {
// Write a Java program to sum values of an array.
        int array [] ={11,12,13,14,15,16,17,18,9,10};
        int sum =0;
        for ( int i : array)
            sum+=i;
        System.out.println("The sum is "+ sum);
    }
}
