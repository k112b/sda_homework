package tasks.Arrays;

public class task13 {
    public static void main(String[] args) {
        // Write a Java program to find the duplicate values of an array of string values.

        String[] array = {"tue", "wed", "thu", "mon", "tue", "wed", "fri"};

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if ((array[i].equals(array[j])) && (i != j)) {
                    System.out.println("Duplicate Element is : " + array[j]);
                }
            }
        }
    }
}

