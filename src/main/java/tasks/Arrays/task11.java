package tasks.Arrays;

import java.util.Arrays;

public class task11 {
    public static void main(String[] args) {
        // Write a Java program to reverse an array of integer values.
        int[] array = {
                3453, 2562, 6353, 1456, 4563,
                1458, 2458, 3562, 1472, 2365,
                7483, 2165, 7954, 5274};
        System.out.println("Original array : "+ Arrays.toString(array));
        for(int i = 0; i < array.length / 2; i++)
        {
            int temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }
        System.out.println("Reverse array : "+ Arrays.toString(array));

    }
}
