package tasks.Arrays;

import java.util.Arrays;

public class task8 {
    public static void main(String[] args) {
        // Write a Java program to copy an array by iterating the array.
        int[] array = {4,7,16,37,53,85,46,28,485,173};
        int[] new_array = new int[10];

        System.out.println("Source Array : "+ Arrays.toString(array));

        for(int i=0; i < array.length; i++) {
            new_array[i] = array[i];
        }
        System.out.println("New Array: "+Arrays.toString(new_array));
    }
}
