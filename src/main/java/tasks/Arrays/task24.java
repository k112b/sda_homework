package tasks.Arrays;

public class task24 {
    public static void main(String[] args) {
        // Write a Java program to find a missing number in an array.
        int total_num;
        int[] numbers = new int[]{7,2,4,8,9,0};
        total_num = 7;
        int expected_num_sum = total_num * ((total_num + 1) / 2);
        int num_sum = 0;
        for (int i: numbers) {
            num_sum += i;
        }
        System.out.print( expected_num_sum - num_sum);
        System.out.print("\n");

    }
}
