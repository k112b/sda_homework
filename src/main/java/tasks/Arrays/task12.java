package tasks.Arrays;

public class task12 {
    public static void main(String[] args) {
        // Write a Java program to find the duplicate values of an array of integer
        //values.
        int[] array = {1, 2, 3, 4, 5, 6, 2, 8};

        for (int i = 0; i < array.length-1; i++)
        {
            for (int j = i+1; j < array.length; j++)
            {
                if ((array[i] == array[j]) && (i != j))
                {
                    System.out.println("Duplicate Element : "+array[j]);
                }
            }
        }

    }
}
