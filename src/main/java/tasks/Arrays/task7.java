package tasks.Arrays;

import java.util.Arrays;

public class task7 {
    public static void main(String[] args) {
        // Write a Java program to remove a specific element from an array.

        int[] array = {5, 10, 65, 83, 3, 6, 656, 172, 209, 9};

        System.out.println("Original Array : "+ Arrays.toString(array));


        int removeIndex = 1;

        for(int i = removeIndex; i < array.length -1; i++){
            array[i] = array[i + 1];
        }

        System.out.println("After removing the second element: "+Arrays.toString(array));

    }
}
