package tasks.Arrays;

import java.util.Arrays;

public class task10 {
    static int max;
    static int min;

    public static void max_min(int array[]) {
        max = array[0];
        min = array[0];
        int len = array.length;
        for (int i = 1; i < len - 1; i = i + 2) {
            if (i + 1 > len) {
                if (array[i] > max) max = array[i];
                if (array[i] < min) min = array[i];
            }
            if (array[i] > array[i + 1]) {
                if (array[i] > max) max = array[i];
                if (array[i + 1] < min) min = array[i + 1];
            }
            if (array[i] < array[i + 1]) {
                if (array[i] < min) min = array[i];
                if (array[i + 1] > max) max = array[i + 1];
            }
        }
    }

    public static void main(String[] args) {
        //  Write a Java program to find the maximum and minimum value of an
        //array.
        int[] array = {2, 1, 6, 24,36,47,85,79,3,10};
        max_min(array);
        System.out.println(" Original Array: "+ Arrays.toString(array));
        System.out.println(" Maximum value for the above array = " + max);
        System.out.println(" Minimum value for the above array = " + min);

    }
}
