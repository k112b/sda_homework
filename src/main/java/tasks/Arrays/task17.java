package tasks.Arrays;

import java.util.Arrays;

public class task17 {
    public static void main(String[] args) {
        // Write a Java program to find the second largest element in an array.

        int[] my_array = {4,7,243,657,89765,34563,7467,4562,724};
        System.out.println("Original numeric array : "+ Arrays.toString(my_array));
        Arrays.sort(my_array);
        int index = my_array.length-1;
        while(my_array[index]==my_array[my_array.length-1]){
            index--;
        }
        System.out.println("Second largest value: " + my_array[index]);
    }
}
