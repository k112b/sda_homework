package tasks.Arrays;

public class task23 {
    static void  equality_checking_two_arrays(int[] my_array1, int[] my_array2)
    {
        boolean  equalOrNot = true;

        if(my_array1.length == my_array2.length)
        {
            for (int i  = 0; i < my_array1.length; i++)
            {
                if(my_array1[i] != my_array2[i])
                {
                    equalOrNot = false;
                }
            }
        }
        else
        {
            equalOrNot  = false;
        }

        if  (equalOrNot)
        {
            System.out.println("Two arrays are equal.");
        }
        else
        {
            System.out.println("Two  arrays are not equal.");
        }
    }
    public static void main(String[] args) {
        // Write a Java program to test the equality of two arrays.

        int[] array1 =  {3, 4, 5, 6, 7};
        int[] array2 =  {3, 4, 5, 6, 7};
        int[] array3 =  {3, 2, 5, 6, 7};

        equality_checking_two_arrays(array1,  array2);
        equality_checking_two_arrays(array1, array3);
    }
}
