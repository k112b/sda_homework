package tasks.Arrays;

import java.util.Arrays;
import java.util.HashSet;

public class task14 {
    public static void main(String[] args) {
        // Write a Java program to find the common elements between two arrays
        //(string values).
        String[] array1 = {"one", "three", "four", "six", "eight", "ten"};

        String[] array2 = {"three", "eleven", "sixty", "eight", "eighteen"};

        System.out.println("Array1 : "+ Arrays.toString(array1));
        System.out.println("Array2 : "+Arrays.toString(array2));

        HashSet<String> set = new HashSet<String>();

        for (int i = 0; i < array1.length; i++)
        {
            for (int j = 0; j < array2.length; j++)
            {
                if(array1[i].equals(array2[j]))
                {
                    set.add(array1[i]);
                }
            }
        }

        System.out.println("Common element : "+(set));


    }
}
