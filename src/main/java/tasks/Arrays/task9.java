package tasks.Arrays;

import java.util.Arrays;

public class task9 {
    public static void main(String[] args) {
        // Write a Java program to insert an element (specific position) into an array.

        int[] array = {1,2,3,4,5,6,7,8,9,10};


        int Index_position = 2;
        int newValue    = 5;

        System.out.println("Original Array : "+ Arrays.toString(array));

        for(int i=array.length-1; i > Index_position; i--){
            array[i] = array[i-1];
        }
        array[Index_position] = newValue;
        System.out.println("New Array: "+Arrays.toString(array));
    }
}
