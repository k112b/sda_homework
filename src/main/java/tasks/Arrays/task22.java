package tasks.Arrays;

public class task22 {
    static void  pairs_value(int inputArray[], int inputNumber)
    {
        System.out.println("Pairs of elements and their sum : ");

        for (int i =  0; i < inputArray.length; i++)
        {
            for (int j  = i+1; j < inputArray.length; j++)
            {
                if(inputArray[i]+inputArray[j] == inputNumber)
                {
                    System.out.println(inputArray[i]+" + "+inputArray[j]+" =  "+inputNumber);
                }
            }
        }
    }

    public static void main(String[] args) {
        // Write a Java program to find all pairs of elements in an array whose sum
        //is equal to a specified number.
        pairs_value(new int[] {7, 4, 2, -4, 1, 5, 6}, 13);

        pairs_value(new int[] {25, -74, 6, 4, 46, 75, 24, 2}, 50);
    }
}
