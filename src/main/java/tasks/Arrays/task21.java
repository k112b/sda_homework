package tasks.Arrays;

import java.util.ArrayList;

public class task21 {
    public static void main(String[] args) {
        // Write a Java program to convert an ArrayList to an array.
        ArrayList<String> list = new ArrayList<String>();

        list.add("Spring");

        list.add("Summer");

        list.add("Autumn");

        list.add("Winter");

        String[]  my_array = new String[list.size()];

        list.toArray(my_array);

        for (String  string : my_array)
        {
            System.out.println(string);
        }


    }
}
