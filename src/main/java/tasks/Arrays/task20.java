package tasks.Arrays;

import java.util.ArrayList;
import java.util.Arrays;

public class task20 {
    public static void main(String[] args) {
        // Write a Java program to convert an array to ArrayList
        String[]  my_array = new String[] {"Spring", "Summer", "Autumn",  "Winter"};
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(my_array));

        System.out.println(list);
    }
}
