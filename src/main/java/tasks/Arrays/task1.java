package tasks.Arrays;

import java.util.Arrays;

public class task1 {
    public static void main(String[] args) {
        // write your code here
        //Write a Java program to sort a numeric array and a string array.
        int[] array1 = {6, 8, 4, 2, 1};
        String[] array2 = {"Spring", "Birds", "Autumn", "Ice", "Rink"};

        System.out.println("Original numeric array : " + Arrays.toString(array1));
        Arrays.sort(array1);
        System.out.println("Sorted numeric array: " + Arrays.toString(array1));
        System.out.println("Original numeric array : " + Arrays.toString(array2));
        Arrays.sort(array2);
        System.out.println("Sorted numeric array: " + Arrays.toString(array2));

    }
}
