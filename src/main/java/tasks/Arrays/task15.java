package tasks.Arrays;

import java.util.Arrays;

public class task15 {
    public static void main(String[] args) {
        //  Write a Java program to find the common elements between two arrays of
        //integers.
        int[] array1 = {5, 8, 4, 9, 5, 9, 2, 15};
        int[] array2 = {16, 8, 63, 2, 4, 7, 4, 1};

        System.out.println("Array1 : "+ Arrays.toString(array1));
        System.out.println("Array2 : "+ Arrays.toString(array2));


        for (int i = 0; i < array1.length; i++)
        {
            for (int j = 0; j < array2.length; j++)
            {
                if(array1[i] == (array2[j]))
                {

                    System.out.println("Common element is : "+(array1[i]));
                }
            }
        }

    }
}
