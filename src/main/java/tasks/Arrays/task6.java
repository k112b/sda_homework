package tasks.Arrays;

public class task6 {
    public static int  findIndex (int[] array, int t) {
        if (array == null) return -1;
        int len = array.length;
        int i = 1;
        while (i < len) {
            if (array[i] == t) return i;
            else i=i+1;
        }
        return -1;
    }


    public static void main(String[] args) {
        // Write a Java program to find the index of an array element.

        int [] array ={14,5,8,34,51,67,45,4,9};
        System.out.println("Index position of 4 is "+ findIndex(array,4) );
    }
}
